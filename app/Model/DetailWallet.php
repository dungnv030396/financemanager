<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailWallet extends Model
{
    protected $fillable =['wallet_id', 'type_id', 'category_id','amount'];

    public function wallet(){
        return $this->belongsTo(Wallet::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
}
