<?php

namespace App\Model;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Category extends Model
{
    protected $fillable = ['name', 'type_id', 'user_id', 'avatar'];
    const PAGINATION_CATEGORY_PAGE = 10;
    const EXPENSE_TYPE_ID =1;
    const INCOME_TYPE_ID =2;

    /**
     * create relation ship with DetailWallet model
     * foreign key category_id and local key id
     */
    public function details()
    {
        return $this->hasOne(DetailWallet::class, 'category_id', 'id');
    }

    /**
     * Create a new category
     */
    public function createNewCategory()
    {
        $type = request('type_name');
        ($type === 'expense') ? $type_id = Category::EXPENSE_TYPE_ID : $type_id = Category::INCOME_TYPE_ID;
        $avatar = \request('avatar_category');
        (is_null($avatar))?$filenameFinal='default.png':$filenameFinal = time() . '.' . $avatar->getClientOriginalName();
        Category::create([
            'name' => request('category_name'),
            'type_id' => $type_id,
            'user_id' => Auth::user()->id,
            'avatar' => $filenameFinal,
        ]);
        if (!is_null($avatar)){
            $avatar->storeAs('public/category_avatar', $filenameFinal);
        }
    }

    /**
     * Filter and get object $listCategories top revenue of categories
     *
     * @return object $listCategories
     */
    public function filterTopRevenue($request)
    {
        request('type_name') == Category::EXPENSE_TYPE_ID?$type_id = request('expense'):$type_id = request('income');
        session()->put('type-name',request('type_name'));
        session()->put('category',$type_id);
        $type_name = request('type_name');
        $start_date = request('start_date');
        $end_date = request('end_date');
        $start_date?$start_date=Carbon::parse($start_date):null;
        $end_date?$end_date=Carbon::parse($end_date)->addHour(23)->addMinute(59)->addSecond(59):null;
        session()->put('start_date',$start_date);
        session()->put('end_date',$end_date);
        $listCategories = DB::table('detail_wallets')
            ->select(DB::raw('detail_wallets.category_id,sum(amount) as total,detail_wallets.type_id,categories.name,categories.created_at,categories.avatar'))
            ->join('categories', 'detail_wallets.category_id', 'categories.id')
            ->join('wallets','detail_wallets.wallet_id','wallets.id');
        if (is_null($start_date) AND !is_null($end_date)) {
            $request->validate([
                'end_date' => 'required|date|date_format:"Y-m-d"',
            ]);
            $listCategories->whereDate('detail_wallets.created_at', '<=', $end_date);
        }elseif(!is_null($start_date) AND is_null($end_date)){
            $request->validate([
                'start_date' => 'required|date|date_format:"Y-m-d"',
            ]);
            $listCategories->whereDate('detail_wallets.created_at', '>', $start_date);
        }elseif (!is_null($start_date) AND !is_null($end_date)){
            $request->validate([
                'start_date' => 'required|date|date_format:"Y-m-d"|before_or_equal:end_date',
                'end_date' => 'required|date|date_format:"Y-m-d"|after_or_equal:start_date',
            ]);
            $listCategories->whereDate('detail_wallets.created_at', '>=', $start_date)
                ->whereDate('detail_wallets.created_at', '<=', $end_date);
        }
        if (!is_null($type_name)) {
            if ($type_name== Category::EXPENSE_TYPE_ID AND !is_null(request('expense'))){
                $listCategories->where('detail_wallets.category_id',request('expense'));
            }elseif($type_name== Category::INCOME_TYPE_ID AND !is_null(request('income'))){
                $listCategories->where('detail_wallets.category_id',request('income'));
            }
            if (is_null(request('expense')) OR is_null(request('income'))){
                $listCategories->where('detail_wallets.type_id', $type_name);
            }
        }
        return $listCategories
            ->where('wallets.user_id',Auth::user()->id)
            ->groupBy('detail_wallets.category_id')
            ->orderByRaw('total DESC')
            ->paginate(Category::PAGINATION_CATEGORY_PAGE);
    }

    /**
     * Edit category by category_id
     *
     */
    public function editCategory(){
        $category = Category::find(request('category_id'));
        $avatar = \request('avatar');
        (is_null($avatar))?$filenameFinal='default.png':$filenameFinal = time() . '.' . $avatar->getClientOriginalName();
        $category->update([
            'name' => request('name'),
            'type_id' => request('type_id'),
            'avatar' => $filenameFinal,
            'updated_at' => Carbon::now(),
        ]);
        if (!is_null($avatar)){
            $avatar->storeAs('public/category_avatar', $filenameFinal);
        }
    }
}
