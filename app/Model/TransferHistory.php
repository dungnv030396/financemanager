<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransferHistory extends Model
{
    protected $fillable = ['user_id', 'wallet_out_id', 'wallet_in_id', 'amount', 'status'];

    public function wallet_out()
    {
        return $this->hasOne(Wallet::class, 'id', 'wallet_out_id');
    }

    public function wallet_in()
    {
        return $this->hasOne(Wallet::class, 'id', 'wallet_in_id');
    }

    /**
     * find object $wallet_out and $wallet_in by wallet_id
     * Update amount_wallet and Create transfer history
     *
     * @return bool
     */
    public function transferMoney()
    {
        $wallet_out = Wallet::find(\request('wallet_out'));
        $wallet_in = Wallet::find(\request('wallet_in'));
        if ($wallet_out->amount_wallet - request('amount') < 0) {
            alert()->warning('Account amount is not enough to perform')->persistent('Close');
            return true;
        }
        $wallet_out->update([
            'amount_wallet' => $wallet_out->amount_wallet - \request('amount'),
        ]);
        $wallet_in->update([
            'amount_wallet' => $wallet_in->amount_wallet + \request('amount'),
        ]);

        TransferHistory::create([
            'user_id' => Auth::user()->id,
            'wallet_out_id' => \request('wallet_out'),
            'wallet_in_id' => \request('wallet_in'),
            'amount' => \request('amount'),
        ]);
    }

    /**
     * Search wallet out name and wallet in name
     *
     * @return object $transferHistory
     */
    public function searchWalletName(){
        $search = trim(request('value'));
        $transferHistory = TransferHistory::whereHas('wallet_out',function ($query) use ($search){
            $query->where('name','like',"%$search%");
        })
            ->orwhereHas('wallet_in',function ($query) use ($search){
                $query->where('name','like',"%$search%");
            })
            ->orwhereDate('created_at','like',"%$search%")
            ->where('user_id',Auth::user()->id)
            ->orderByRaw('created_at DESC')
            ->paginate(Category::PAGINATION_CATEGORY_PAGE);
        return $transferHistory;
    }
}
