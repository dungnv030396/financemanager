<?php

namespace App\Model;

use App\Mail\CreateNewAccount;
use App\Mail\ForgotPassword;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password', 'address', 'phone', 'birthday', 'status', 'email_verified_at'
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $dates = ['labels', 'currentTime'];

    const NUMBER_OF_MONTH_IN_CHART = 7;
    const NUMBER_OF_TOKEN_LENGTH = 20;

    /**
     * create a new token (random 20 characters)
     * @return string
     */
    public function createNewToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < User::NUMBER_OF_TOKEN_LENGTH; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Check email exist or not
     * Create a new account
     * and send mail to user to confirm email
     * @return bool
     */
    public function createNewAccount()
    {
        $user = new User();
        $count = User::where('email', request('email'))->first();
        if ($count) {
            return false;
        } else {
            $user->name = request('name');
            $user->email = trim(request('email'));
            $user->address = request('address');
            $user->phone = request('phone');
            $user->avatar = 'default.png';
            $user->birthday = Carbon::createFromFormat('d-m-Y', \request('birthday'));
            $user->password = bcrypt(request('password'));
            $user->token_confirm_password = $this->createNewToken();
            $user->save();
            Mail::to($user)->send(new CreateNewAccount($user));

            return true;
        }
    }

    /**
     * Confirm email by $token
     * update user account
     *
     * @param $token
     * @return bool
     */
    public function confirmEmail($token)
    {
        $user = User::where('token_confirm_password', $token)->first();
        if ($user) {
            $user->email_verified_at = 1;
            $user->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check email is exist or not
     * Create a token save in password_resets table
     * and send mail to user
     * @return bool
     */
    public function forgotPassword()
    {
        $email = request('email');
        $randomString = $this->createNewToken();
        $user = User::where('email', $email)->first();
        if ($user) {
            $token = PasswordReset::where('user_id', $user->id)->first();
            if ($token) {
                $token->token = Hash::make($randomString);
                $token->status = 1;
                $token->save();
            } else {
                PasswordReset::create([
                    'user_id' => $user->id,
                    'token' => Hash::make($randomString),
                    'status' => 1
                ]);
            }
            $data = [
                'token' => $randomString,
                'user' => $user,
            ];
            Mail::to($user)->send(new ForgotPassword($data));
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check token request in password_resets table is exist or not
     *
     * @param $token
     * @return mixed
     */
    public function verificationToken($token)
    {
        $pResets = PasswordReset::all();
        foreach ($pResets as $item) {
            if (Hash::check($token, $item->token) AND $item->status == 1) {
                $user = User::find($item->user_id);
                return $user;
            }
        }
    }

    /**
     * Change password after forgot password by email
     * @return bool
     */
    public function changePasswordByEmail()
    {
        $pResets = PasswordReset::where('user_id', request('user_id'))->first();
        $user = User::find(request('user_id'));
        if ($user) {
            $user->password = Hash::make(request('password'));
            $user->save();
            $pResets->status = 0;
            $pResets->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * login to system with root account
     * @return bool
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password'), 'status' => 1, 'email_verified_at' => 1])) {
            return true;
        }
        return false;
    }

    /**
     * logout account
     * clear session
     */
    public function logout()
    {
        Auth::logout();
    }

    /**
     * change password by current password
     * @return bool
     */
    public function changePassword()
    {
        if (Hash::check(\request('current_password'), Auth::user()->password)) {
            Auth::user()->fill([
                'password' => Hash::make(request('password'))
            ])->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Change user's information by user id
     *
     */
    public function changeInformation()
    {
        User::find(Auth::user()->id)->update([
            'name' => request('name'),
            'address' => request('address'),
            'phone' => request('phone'),
            'birthday' => Carbon::createFromFormat('d-m-Y', \request('birthday')),
        ]);
    }

    /**
     * get last 7 month from current month
     * @return array
     */
    public function getLabels()
    {
        $label = array();
        $currentTime = Carbon::now()->format('m/Y');
        for ($i = 1; $i <= User::NUMBER_OF_MONTH_IN_CHART; $i++) {
            array_push($label, $currentTime);
            $currentTime = Carbon::now()->startOfMonth()->subMonth($i)->format('m/Y');
        }
        return $label;
    }
    /**
     * get income revenue last 7month from current month
     * @return array
     */
    public function getRevenueByTypeForLineChart($type){
        $currentMonth = Carbon::now()->month;
        $revuene = array();
        for ($i = 1; $i <= User::NUMBER_OF_MONTH_IN_CHART; $i++) {
            $detailByMonth = DetailWallet::where('type_id', $type)
                ->where('wallets.user_id',Auth::user()->id)
                ->whereMonth('detail_wallets.created_at', $currentMonth)
                ->join('wallets','wallets.id','detail_wallets.wallet_id')
                ->get();
            $total =0;
            foreach ($detailByMonth as $item){
                $total += $item->amount;
            }
            array_push($revuene,$total);
            $currentMonth = Carbon::now()->startOfMonth()->subMonth($i)->month;
        }
        return $revuene;
    }

    /**
     * get object $listCategories by $type_id, date range,default current month
     * @param $type_id
     * @return object $listCategories
     */
    public function getRevenueByType($type_id,$request){
        $start_date = request('start_date');
        $end_date = request('end_date');
        $start_date?$start_date=Carbon::parse($start_date):null;
        $end_date?$end_date=Carbon::parse($end_date)->addHour(23)->addMinute(59)->addSecond(59):null;

        session()->put('start_date_chart',$start_date);
        session()->put('end_date_chart',$end_date);
        $listCategories = DB::table('detail_wallets')
            ->select(DB::raw('detail_wallets.category_id,sum(amount) as total,detail_wallets.type_id,categories.name,categories.created_at,categories.avatar,wallets.id'))
            ->join('categories', 'detail_wallets.category_id', 'categories.id')
            ->join('wallets', 'wallets.id', 'detail_wallets.wallet_id');

        if (is_null($start_date) AND !is_null($end_date)) {
            $request->validate([
                'end_date' => 'required|date|date_format:"Y-m-d"',
            ]);
            $listCategories->whereDate('detail_wallets.created_at', '<=', $end_date);
        }elseif(!is_null($start_date) AND is_null($end_date)){
            $request->validate([
                'start_date' => 'required|date|date_format:"Y-m-d"',
            ]);
            $listCategories->whereDate('detail_wallets.created_at', '>=', $start_date);
        }elseif (!is_null($start_date) AND !is_null($end_date)){
            $request->validate([
                'start_date' => 'required|date|date_format:"Y-m-d"|before_or_equal:end_date',
                'end_date' => 'required|date|date_format:"Y-m-d"|after_or_equal:start_date',
            ]);
            $listCategories->whereDate('detail_wallets.created_at', '>=', $start_date)
                ->whereDate('detail_wallets.created_at', '<=', $end_date);
        }
        return $listCategories->where('detail_wallets.type_id',$type_id)
            ->where('wallets.user_id',Auth::user()->id)
            ->groupBy('detail_wallets.category_id')
            ->orderByRaw('total DESC')
            ->paginate(Category::PAGINATION_CATEGORY_PAGE);
    }
    /**
     * Update avatar and store image to /storage/public/avatar
     */
    public function updateAvatar(){
            $avatar = \request('avatar');
            $filenameFinal = time().'.'.$avatar->getClientOriginalName();
            $user = User::find(Auth::user()->id);
            $user->avatar = $filenameFinal;
            $user->save();
            $avatar->storeAs('public/avatar', $filenameFinal);
    }
}
