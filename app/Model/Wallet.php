<?php

namespace App\Model;

use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Wallet extends Model
{
    protected $fillable = [
        'user_id', 'name', 'status', 'notes', 'amount_wallet'
    ];
    const PAGINATION_LIST_WALLETS = 5;

    public function details()
    {
        return $this->hasMany(DetailWallet::class, 'wallet_id', 'id');
    }

    /**
     * Create a new wallet
     *
     * @return Wallet
     */
    public function createNewWallet()
    {
        $wallet = new Wallet();
        $wallet->user_id = Auth::user()->id;
        $wallet->name = trim(request('name'));
        $wallet->notes = trim(request('notes'));
        $wallet->amount_wallet = trim(request('amount_wallet'));
        $wallet->save();
        return $wallet;
    }

    /**
     * get list wallets and search wallet name
     *
     * @param $value
     * @return mixed
     */
    public function getListWallets($value)
    {
        if ($value === 'all') {
            $wallets = Wallet::where('user_id', Auth::user()->id)
                ->where('status', 1)
                ->paginate(Wallet::PAGINATION_LIST_WALLETS);
        } else {
            $wallets = Wallet::where(function ($query) use ($value) {
                $query->where('name', 'like', "%$value%");
            })
                ->where('user_id', Auth::user()->id)
                ->where('status', 1)
                ->paginate(Wallet::PAGINATION_LIST_WALLETS);
        }

        return $wallets;
    }

    /**
     * update wallet's information
     *
     * @param $request
     * @return null
     */
    public function updateWallet($request)
    {
        $wallet = Wallet::find(request('wallet_id'));
        $request->validate([
            'name' => 'required|between:1,30',
            'notes' => 'required|between:1,200',
        ]);
        $wallet->update([
            'name' => request('name'),
            'notes' => request('notes'),
            'updated_at' => Carbon::now(),
        ]);

        return $wallet;
    }

    /**
     * Add new category in the wallet
     *
     * @param $request
     * @return bool
     */
    public function updateCategoriesInWallet($request){
        $type = request('type');
        ($type === 'expense') ? $type_id = Category::EXPENSE_TYPE_ID : $type_id = Category::INCOME_TYPE_ID;
        ($type === 'expense') ? $category_id = request('expense') : $category_id = request('income');
        $wallet_id = request('wallet_id');
        if (!is_null($category_id)) {
            $request->validate([
                'amount' => 'required|numeric|digits_between:1,15',
                'type' => 'required',
            ]);
            DetailWallet::create([
                'wallet_id' => $wallet_id,
                'type_id' => $type_id,
                'category_id' => $category_id,
                'amount' => request('amount')
            ]);
        }else{
            return true;
        }
    }
}
