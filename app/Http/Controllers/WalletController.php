<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWalletRequest;
use App\Model\Category;
use App\Model\DetailWallet;
use App\Model\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    /**
     * User open modal create category
     *
     * @return categories\create-new-categories.blade.php
     */
    public function viewCreateWalletPage(){
        return view('wallet.create-new-wallet');
    }

    /**
     * User create a new wallet
     *
     * @param CreateWalletRequest $request
     * @return \wallet\create-new-wallet.blade.php
     */
    public function createNewWallet(CreateWalletRequest $request){
       $wallet = new Wallet();
       $res = $wallet->createNewWallet();
       if ($res){
           alert()->success('Create new wallet successful')->persistent('Close');
           return redirect()->route('manage.wallets.page','all')->with('createWalletSuccess','message');
       }else{
           return view('layouts.not-found');
       }
    }

    /**
     * User redirect to management wallet page
     * and get object $wallets from function getListWallets at Model Wallet
     *
     * @param $value
     * @return \wallet\manager-wallets.blade.php
     */
    public function viewManageWalletsPage($value){
        $wallet = new Wallet();
        $wallets = $wallet->getListWallets($value);
        return view('wallet.manager-wallets',compact('wallets'));
    }

    /**
     * User delete Wallet ( change status 1 => 0)
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deleteWallet(){
        $wallet_id = \request('id');
        $wallet = Wallet::find($wallet_id);
        if ($wallet){
            $wallet->update([
               'status' => 0,
            ]);
        }else{
            return view('layouts.not-found');
        }
    }

    /**
     * User redirect to Edit wallet page
     * get object $wallet by id,$dWallet, and object $allCategories
     * passing that to view
     * @param $id
     * @return \wallet\edit-wallet.blade.php
     */
    public function viewDetailWallet($id){
        $wallet = Wallet::find($id);
        return view('wallet.edit-wallet',compact('wallet'));
    }

    /**
     * User update information of wallet
     *
     * @param Request $request
     * @return \wallet\edit-wallet.blade.php
     */
    public function updateWallet(Request $request){
        $wallet = new Wallet();
        $res = $wallet->updateWallet($request);
        if ($res){
            return redirect()->back()->with('updateWalletSuccess','Update wallet information successful');
        }else{
            alert()->error('Please choice one category')->persistent('Close');
            return redirect()->back()->with('updateWalletFail','message');
        }
    }

    /**
     * User search wallet by name
     *
     * @return \wallet\manager-wallets.blade.php
     */
    public function searchWallet(){
        $wallet = new Wallet();
        $search = request('value');
        (is_null($search))?$search='all':$search = request('value');
        $wallets = $wallet->getListWallets($search);
        return redirect()->route('manage.wallets.page',$search)->with(compact('wallets'));
    }

    /**
     * User update categories in the wallet
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewDetailCategoriesInWallet($id){
        $allCategories = Category::whereNull('user_id')->orwhere('user_id',Auth::user()->id)->get();
        $wallet = Wallet::find($id);
        $dWallet = DetailWallet::where('wallet_id',$id)->paginate(Wallet::PAGINATION_LIST_WALLETS);
        $dWalletAll = DetailWallet::where('wallet_id',$id)->get();
        $total = 0;
        foreach ($dWalletAll as $item){
            if ($item->type_id ==1){
                $total -= $item->amount;
            }else{
                $total += $item->amount;
            }
        }

        return view('wallet/edit-categories-in-wallet',compact('dWallet','wallet','allCategories','total'));
    }

    /**
     * User add new category to Wallet
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCategoriesInWallet(Request $request){
        $wallet = new Wallet();
        $res = $wallet->updateCategoriesInWallet($request);
        if (!$res){
            alert()->success('Update category successful!')->persistent('Close');
            return redirect()->back()->with('updateCategorySuccess','message');
        }else{
            alert()->error('Please choice category!')->persistent('Close');
            return redirect()->back()->with('updateCategorySuccess','message');
        }
    }
}
