<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginPasswordRequest;
use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * User login with root account
     *
     * @param LoginPasswordRequest $request
     * @return \UserViews\profile.blade.php
     */
    public function login(LoginPasswordRequest $request){
        $user = new User();
        $res = $user->login();
        if ($res){
            return redirect()->route('profile');
        }else{
            return redirect()->back()->withErrors('Login fail,Please login again');
        }
    }

    /**
     * User logout account
     *
     * @return \UserViews\index.blade.php
     */
    public function doLogout(){
        $user = new User();
        $res = $user->logout();
        if (!$res){
            return redirect()->route('home');
        }
    }

    /**
     * User redirect to Google Provider
     * @return mixed
     */
    public function redirectToProviderGM(){
        return Socialite::driver('google')->redirect();
    }

    /**
     * User login with google account
     *
     * @return \UserViews\profile.blade.php
     */
    public function handleProviderCallbackGM(){
        $userSocialite = Socialite::driver('google')->stateless()->user();
        $findUserSocialite = User::where('email',$userSocialite->email)->first();
        if($findUserSocialite){
            Auth::login($findUserSocialite);
            return redirect()->route('profile');
        }else{
            $user = new User();
            $user->name = $userSocialite->name;
            $user->email = $userSocialite->email;
            $user->email_verified_at = 1;
            $user->avatar = $userSocialite->avatar;
            $user->password = bcrypt('123456');
            $user->save();
            Auth::login($user);
            return redirect()->route('profile');
        }
    }
}
