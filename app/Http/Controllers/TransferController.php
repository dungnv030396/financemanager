<?php

namespace App\Http\Controllers;

use App\Http\Requests\TransferMoneyRequest;
use App\Model\Category;
use App\Model\TransferHistory;
use App\Model\Wallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransferController extends Controller
{
    /**
     * User redirect to transfer page and get object $wallets passing that to view
     *
     * @return \transfer\transfer.blade.php
     */
    public function viewTransferPage(){
        $wallets = Wallet::where('user_id',Auth::user()->id)
            ->where('status',1)
            ->get();
        return view('transfer/transfer',compact('wallets'));
    }

    /**
     * User transfer money between wallets
     *
     * @param TransferMoneyRequest $request
     * @return \transfer\transfer.blade.php
     */
    public function transferMoney(TransferMoneyRequest $request){
        $transferHistory = new TransferHistory();
        $res = $transferHistory->transferMoney();
        if (!$res){
            alert()->success('Transfer money success')->persistent('Close');
            return redirect()->back()->with('transferMessage','message');
        }else{
            return redirect()->back()->with('transferMessage', 'message');
        }
    }

    /**
     * User redirect to transfer history page
     * and get object $transferHistory passing that to view
     *
     * @return \transfer\transfer-history.blade.php
     */
    public function viewTransferHistory(){
        $transferHistory = TransferHistory::where('user_id',Auth::user()->id)
            ->orderByRaw('created_at DESC')
            ->paginate(Category::PAGINATION_CATEGORY_PAGE);
        return view('/transfer/transfer-history',compact('transferHistory'));
    }

    /**
     * User delete transfer history by id
     */
    public function deleteTransferHistory(){
        $history_id = \request('id');
        TransferHistory::find($history_id)->delete();
    }

    /**
     * User search transfer history name
     *
     * @return \transfer\transfer-history.blade.php
     */
    public function searchWalletName(){
        $tHistory = new TransferHistory();
        $transferHistory = $tHistory->searchWalletName();

        return view('transfer/transfer-history',compact('transferHistory'));
    }
}
