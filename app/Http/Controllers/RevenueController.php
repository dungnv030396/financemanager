<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RevenueController extends Controller
{
    /**
     * random color
     *
     * @return string
     */
    function random_color() {
        $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        return $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
    }

    /**
     * get data for chart
     *
     * @return \revenue\pie-chart.blade.php
     */
    public function viewPieChart(Request $request){
        $user = new User();
        $objExpenseRevenue = $user->getRevenueByType(Category::EXPENSE_TYPE_ID,$request);
        $objIncomeRevenue = $user->getRevenueByType(Category::INCOME_TYPE_ID,$request);
        $pieDataArrExpense =[];
        foreach ($objExpenseRevenue as $item){
            array_push($pieDataArrExpense,[
               'value' =>  $item->total,
                'color' => $this->random_color(),
                'highlight' => $this->random_color(),
                'label' => $item->name,
            ]);
        }
        $pieDataArrIncome =[];
        foreach ($objIncomeRevenue as $item){
            array_push($pieDataArrIncome,[
                'value' =>  $item->total,
                'color' => $this->random_color(),
                'highlight' => $this->random_color(),
                'label' => $item->name,
            ]);
        }
        $labels = $user->getLabels();
        $expense = $user->getRevenueByTypeForLineChart(Category::EXPENSE_TYPE_ID);
        $income = $user->getRevenueByTypeForLineChart(Category::INCOME_TYPE_ID);
        $newLabels = array_reverse($labels);
        $newIncome = array_reverse($income);
        $newExpense = array_reverse($expense);
        $pieDataExpenseJson = json_encode($pieDataArrExpense);
        $pieDataIncomeJson = json_encode($pieDataArrIncome);
        $json_labels = json_encode($newLabels);
        $json_expense = json_encode($newExpense);
        $json_income = json_encode($newIncome);

        $dateRange = \request('value-date');
        return view('/revenue/pie-chart',compact('pieDataExpenseJson','pieDataIncomeJson',
            'json_labels','json_expense','json_income','dateRange','pieDataArrExpense','pieDataArrIncome'));
    }
}
