<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\EditCategoryRequest;
use App\Http\Requests\FilterTopRevenueRequest;
use App\Model\Category;
use App\Model\DetailWallet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends Controller
{
    /**
     * User create new category
     *
     * @param CreateCategoryRequest $request
     * @return categories\manager-categories.blade.php
     */
    public function createNewCategory(CreateCategoryRequest $request)
    {
        $category = new Category();
        $res = $category->createNewCategory();
        if (!$res) {
            return redirect()->back()->with('createCategorySuccess', 'Create category successful.');
        } else {
            return redirect()->back()->withErrors('Create category fail.');
        }
    }

    /**
     * User redirect to management categories page with object $categories
     *
     * @return categories\manager-categories.blade.php
     */
    public function viewManageCategories()
    {

        $categories = Category::where('user_id', Auth::user()->id)
            ->orderByRaw('created_at DESC')
            ->paginate(Category::PAGINATION_CATEGORY_PAGE);

        return view('/categories/manager-categories', compact('categories'));
    }

    /**
     * User delete category by id
     */
    public function deleteCategory()
    {
        $category_id = \request('id');
        Category::find($category_id)->delete();
        DetailWallet::where('category_id',$category_id)->delete();
    }

    /**
     * User search category by name
     *
     * @return categories\manager-categories.blade.php
     */
    public function searchCategories()
    {
        $search = \request('value');
        $categories = Category::where(function ($query) use ($search) {
            $query->where('name', 'like', "%$search%");
        })
            ->where('user_id', Auth::user()->id)
            ->orderByRaw('created_at DESC')
            ->paginate(Category::PAGINATION_CATEGORY_PAGE);

        return view('categories/manager-categories', compact('categories'));
    }

    /**
     * View top Revenue of categories
     * get object $listCategories from filterTopRevenue() function at Category Model
     * @return \categories\top-revenue.blade.php
     */
    public function filterTopRevenue(Request $request){
        $category = new Category();
        $allCategories = Category::whereNull('user_id')->orwhere('user_id',Auth::user()->id)->get();
        $listCategories = $category->filterTopRevenue($request);
        return view('/categories/top-revenue',compact('listCategories','allCategories'));
    }

    /**
     * User edit their created categories
     *
     * @param EditCategoryRequest $request
     * @return \categories\manager-categories.blade.phh
     */
    public function editCategory(EditCategoryRequest $request){
        $category = new Category();
        $res = $category->editCategory();
        if (is_null($res)){
            alert()->success('Update Category Successful')->persistent('Close');
           return redirect()->back()->with('updateCategorySuccess','Update Category Successful');
        }else{
            return redirect()->back()->withErrors('not good');
        }
    }
}
