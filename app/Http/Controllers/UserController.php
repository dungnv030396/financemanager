<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeInformationRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\UpdateAvatarImage;
use App\Http\Requests\ValidationRegister;
use App\Model\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Redirect to index page
     *
     * @return \UserViews\index.blade.php
     */
    public function viewIndexPage(){
        return view('UserViews.index');
    }
    /**
     * User redirect to forgot password page
     *
     * @return\UserViews\forgot-password-page.blade.php
     */
    public function viewForgotPassword(){
        return view('UserViews.forgot-password-page');
    }

    /**
     * User redirect to register page
     * @return \UserViews\register.blade.php
     */
    public function viewRegister(){
        return view('UserViews.register');
    }

    /**
     * User create a new account
     *
     * @param ValidationRegister $register
     * @return \UserViews\register.blade.php
     */
    public function createNewAccount(ValidationRegister $register){
        $user = new User();
        $res = $user->createNewAccount();
        if ($res){
            return redirect()->back()->with('createSuccess','Create new account successful, Please confirm your email before login to system');
        }else{
            return redirect()->back()->withErrors('The email was exist');
        }
    }

    /**
     * User confirm their email
     *
     * @param $token
     * @return \UserViews\index.blade.php
     */
    public function confirmEmail($token){
        $user = new User();
        $res = $user->confirmEmail($token);
        if ($res){
            return redirect()->route('home')->with('confirmSuccess','Email confirmation successful');
        }else{
            return view('layouts.not-found');
        }
    }

    /**
     * User forgot password by email
     *
     * @return \UserViews\forgot-password-page.blade.php
     */
    public function forgotPassword(){
        $user = new User();
        $res = $user->forgotPassword();
        if ($res){
            return redirect()->back()->with('emailFound','Please check your email and change password');
        }else{
            return redirect()->back()->withErrors('This email does not exist');
        }
    }
    /**
     * System verification token and redirect to change password page
     */
    public function verificationToken($token){
        $user = new User();
        $res = $user->verificationToken($token);
        if ($res){
            return view('UserViews.change-pass-page-by-mail',compact('res'));
        }else{
            return view('layouts.not-found');
        }
    }

    /**
     * User change password after system verification token and redirect to change password page
     *
     * @param ChangePasswordRequest $request
     * @return \UserViews\index.blade.php
     */
    public function changePasswordByEmail(ChangePasswordRequest $request){
        $user = new User();
        $res = $user->changePasswordByEmail();
        if($res){
            return redirect()->route('home')->with('changeSuccess','Change Password Successful');
        }else{
            return view('layouts.not-found');
        }
    }

    /**
     * User redirect to profile page
     *
     * @return \UserViews\profile.blade.php
     */
    public function viewProfile(){
        return view('UserViews.profile');
    }

    /**
     * User change password with current password
     *
     * @param ChangePasswordRequest $request
     * @return \UserViews\profile.blade.php
     */
    public function changePassword(ChangePasswordRequest $request){
        $user = new User();
        $res = $user->changePassword();
        if ($res){
            return redirect()->back()->with('changePassSuccess','Change password successful');
        }else{
            return redirect()->back()->withErrors('The current password is incorrect');
        }
    }

    /**
     * User change information
     *
     * @param ChangeInformationRequest $request
     * @return \UserViews\profile.blade.php
     */
    public function changeInformation(ChangeInformationRequest $request){
        $user = new User();
        $res = $user->changeInformation();
        if (!$res){
            return redirect()->back()->with('changeInforSuccess','Change Information Successful');
        }else{
            return view('layouts.not-found');
        }
    }

    /**
     * User update their avatar
     * @param UpdateAvatarImage $avatarImage
     * @return \views\UserViews\profile.blade.php
     */
    public function updateAvatar(UpdateAvatarImage $avatarImage){
        $user = new User();
        $res = $user->updateAvatar();
        if(is_null($res)){
            return redirect()->back();
        }else{
            return redirect()->back()->withErrors('Something wrong');
        }
    }
}
