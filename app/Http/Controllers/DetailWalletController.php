<?php

namespace App\Http\Controllers;

use App\Model\DetailWallet;
use Illuminate\Http\Request;

class DetailWalletController extends Controller
{
    /**
     * User delete category in the Wallet
     */
    public function deleteCategory(){
        DetailWallet::find(\request('id'))->delete();
    }
}
