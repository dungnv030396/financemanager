<?php

namespace App\Http\Middleware;

use App\Model\Category;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckForCreateCategory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name = request('category_name');
        $categories = Category::whereNull('user_id')->orwhere('user_id',Auth::user()->id)->get();
        foreach ($categories as $category){
            if ($category->name === $name){
                alert()->error('The category name was exist')->persistent('Close');
                return redirect()->back()->with('existCreateName','message');
            }
        }
        return $next($request);
    }
}
