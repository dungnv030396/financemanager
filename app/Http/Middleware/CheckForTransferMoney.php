<?php

namespace App\Http\Middleware;

use App\Model\Wallet;
use Closure;

class CheckForTransferMoney
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $wallet_out = Wallet::find(\request('wallet_out'));
        $wallet_in = Wallet::find(\request('wallet_in'));

        if (empty($wallet_out) OR empty($wallet_in)){
            alert()->warning('Something Wrong! Please reload and try again')->persistent('Close');
            return redirect()->back()->with('transferMessage','message');
        }
        elseif (\request('wallet_out') == \request('wallet_in')){
            alert()->warning('Sorry! Can not transfer money with the same account')->persistent('Close');
            return redirect()->back()->with('transferMessage','message');
        }

        return $next($request);
    }
}
