<?php

namespace App\Http\Middleware;

use App\Model\Wallet;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckIdForViewDetailWallet
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $wallet = Wallet::where('id',request('id'))
            ->where('user_id',Auth::user()->id)
            ->where('status',1)
            ->get();

        if (count($wallet)==0){
            alert()->error('The url not exist or The wallet was deleted')->persistent('Close');
            return redirect()->back()->with('errorID','message');
        }else{
            return $next($request);
        }
    }
}
