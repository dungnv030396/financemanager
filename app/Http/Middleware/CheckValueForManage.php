<?php

namespace App\Http\Middleware;

use Closure;

class CheckValueForManage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $value = request('value');
        $rules = ['all','expense','income'];
        if (!in_array($value,$rules)){
            alert()->error('The url not exist','Oops!')->persistent();
            return redirect()->back()->with('errorURL','message');
        }
        return $next($request);
    }
}
