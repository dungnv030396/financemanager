<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidationRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|between:1,40',
            'email' => 'required|between:5,50',
            'address' => 'required|between:1,200',
            'phone' => 'required|digits:10',
            'password' => 'required|between:6,30|confirmed',
            'birthday' => 'required',
        ];
    }
}
