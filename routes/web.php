<?php

Route::get('index','UserController@viewIndexPage')->name('home');

//Login and logout
Route::get('google', 'LoginController@redirectToProviderGM')->name('login.google');
Route::get('callback/google', 'LoginController@handleProviderCallbackGM');
Route::post('login','LoginController@login')->middleware('guest')->name('login');
Route::get('logout','LoginController@doLogout')->middleware('auth')->name('logout');

//forgot password and change password
Route::get('forgot-password','UserController@viewForgotPassword')->name('view.forgot.password.page');
Route::post('forgot-password','UserController@forgotPassword')->name('forgot.password');
Route::post('change-password/by-email','UserController@changePasswordByEmail')->name('change.password.by.email');
Route::post('change-password/by-current-password','UserController@changePassword')->middleware('auth')->name('change.password');

//register,confirm email and verification token
Route::get('profile','UserController@viewProfile')->middleware('auth')->name('profile');
Route::post('change-information','UserController@changeInformation')->middleware('auth')->name('change.information');
Route::get('verification-token/{token}','UserController@verificationToken')->name('verification.token');
Route::get('register','UserController@viewRegister')->name('view.register.page');
Route::post('register','UserController@createNewAccount')->name('register');
Route::get('confirm-email/{token}','UserController@confirmEmail')->name('confirm.email');
Route::post('update-avatar','UserController@updateAvatar')->middleware('auth')->name('update.avatar');

//wallet
Route::get('create-new-wallet','WalletController@viewCreateWalletPage')->middleware('auth')->name('create.new.wallet.page');
Route::post('create-new-wallet','WalletController@createNewWallet')->middleware('auth')->name('create.new.wallet');
Route::get('manage-wallets/{value}','WalletController@viewManageWalletsPage')->middleware('auth')->name('manage.wallets.page');
Route::post('delete-wallet','WalletController@deleteWallet')->middleware('auth')->name('delete.wallet');
Route::get('view-detail-wallet/{id}','WalletController@viewDetailWallet')->middleware('auth','checkIdForViewDetailWallet')->name('view.detail.wallet');
Route::post('update-wallet','WalletController@updateWallet')->middleware('auth')->name('update.wallet');
Route::post('search-wallet','WalletController@searchWallet')->middleware('auth')->name('search.wallet');
Route::get('view-categories-in-wallet/{id}','WalletController@viewDetailCategoriesInWallet')->middleware('auth','checkIdForViewDetailWallet')->name('view.categories.in.wallet');
Route::post('update-categories-in-wallet','WalletController@updateCategoriesInWallet')->middleware('auth')->name('update.categories.in.wallet');

//categories
Route::post('create-category','CategoryController@createNewCategory')->middleware('auth','checkForCreateCategory')->name('create.new.category');
Route::post('delete-category-in-wallet','DetailWalletController@deleteCategory')->middleware('auth')->name('delete.category.in.wallet');
Route::get('manager-categories-page','CategoryController@viewManageCategories')->middleware('auth')->name('view.manage.categories');
Route::post('delete-category','CategoryController@deleteCategory')->middleware('auth')->name('delete.category');
Route::post('search-categories','CategoryController@searchCategories')->middleware('auth')->name('search.categories');
Route::get('view-top-revenue-categories','CategoryController@filterTopRevenue')->middleware('auth')->name('view.top.revenue.categories');
Route::post('view-top-revenue-categories','CategoryController@filterTopRevenue')->middleware('auth')->name('view.top.revenue.categories.by.filter');
Route::post('edit-category','CategoryController@editCategory')->middleware('auth','CheckForEditCategory')->name('edit.categories');

//transfer money
Route::get('transfer-money','TransferController@viewTransferPage')->middleware('auth')->name('view.transfer.page');
Route::post('transfer-money','TransferController@transferMoney')->middleware('auth','CheckForTransferMoney')->name('view.transfer.money');
Route::get('transfer-history-page','TransferController@viewTransferHistory')->middleware('auth')->name('view.transfer.history');
Route::post('delete-transfer-history','TransferController@deleteTransferHistory')->middleware('auth')->name('delete.transfer.history');
Route::post('transfer-history-page','TransferController@searchWalletName')->middleware('auth')->name('search.wallet.name.in.history');

//revenue chart
Route::get('view-revenue-chart','RevenueController@viewRevenueChart')->middleware('auth')->name('view.revenue.chart');
Route::get('view-pie-chart','RevenueController@viewPieChart')->middleware('auth')->name('view.pie.chart');
Route::post('view-pie-chart','RevenueController@viewPieChart')->middleware('auth')->name('view.pie.chart.filter');