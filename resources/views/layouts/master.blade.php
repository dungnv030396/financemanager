<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="{{asset('js/jquery.min.js')}}"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="{{asset('css/master.css')}}">
<!------ Include the above in your HEAD tag ---------->
<header>
    <title>Finance Manage</title>
</header>
<body>
<div id="throbber" style="display:none; min-height:120px;"></div>
<div id="noty-holder"></div>

<div id="wrapper">

    <!-- Navigation -->
    @include('layouts.nav')

    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-md-12 dashhead">
                @yield('content')
            </div>
        </div>
    </div>

</div><!-- /#wrapper -->
</body>