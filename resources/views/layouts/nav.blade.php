<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars" aria-hidden="true" style="color: white;"></i>
        </button>
        <div class="navbar-brand">
        </div>
    </div>

    <form action="" class="navbar-form navbar-left">
        <div class="input-group">
            <div class="input-group-btn">
                <button class="btn  search-btn-icon">
                    <i class="fa fa-search" aria-hidden="true"></i>
                </button>
            </div>
            <input type="Search" placeholder="Search..." class="form-control-serch search-box"/>
        </div>
    </form>

    <!-- Top Menu Items -->
    <div class="items">
        <ul class="nav navbar-right top-nav" style="margin-right: 20px;">
            <li class="editpro">
                <i class="fasett fa-cog" aria-hidden="true" class="menu-button" id="menu-button"></i>
                @if(\Illuminate\Support\Facades\Auth::check())
                    <h5 class="pull-left login-person-head">Welcome: <a href="{{route('profile')}}">{{\Illuminate\Support\Facades\Auth::user()->name}}</a><span style="margin-left: 40px"><a
                                    href="{{route('logout')}}">Logout</a></span></h5>
                @endif
            </li>
        </ul>
    </div>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse"
         style="background-color: #471481; border:1px solid white;">
        <ul class="nav side-nav">
            <a href="#"><img class="logostyle"
                             src="https://vignette.wikia.nocookie.net/nationstates/images/2/29/WS_Logo.png/revision/latest?cb=20080507063620"
                             alt="LOGO"></a>
            <li>
                <a class="active" href="{{route('manage.wallets.page','all')}}" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-home" aria-hidden="true"></i>  
                    <span style="color:white;">Wallet Management</span></a>
            </li>
            <li>
                <a class="#" href="{{route('view.manage.categories')}}" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-user-o" aria-hidden="true"></i>
                    <span style="color:white;">Categories Management</span></a>
            </li>
            <li>
                <a class="#" href="{{route('view.top.revenue.categories')}}" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-user-o" aria-hidden="true"></i>
                    <span style="color:white;">Transactions Management</span></a>
            </li>
            <li>
                <a class="#" href="{{route('view.transfer.history')}}" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-calendar" aria-hidden="true"></i>
                      <span style="color:white;">Transfer History</span></a>
            </li>
            <li>
                <a class="#" href="{{route('view.pie.chart')}}" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-envelope" aria-hidden="true"></i>
                      <span style="color:white;">Revenue Management</span></a>
            </li>
        </ul>
    </div>
</nav>
