<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link rel="stylesheet" type="text/css" href="css/css-register">
<script src="{{asset('js/jquery.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

<!------ Include the above in your HEAD tag ---------->
<div class="container-fluid">
    <div class="row" style="margin: 50px 10px 50px 10px">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title">Finance Manage</h1>
                <hr>
            </div>
        </div>
        <h2 class="title text-left text-blue">Register New Account Page</h2>
        <div class="form-group">
            @if (\Illuminate\Support\Facades\Session::has('createSuccess'))
                <div class="alert alert-success">
                    <ul>
                        {{\Illuminate\Support\Facades\Session::get('createSuccess')}}
                    </ul>
                </div>
            @endif
        </div>
        <div class="main-login main-center">
            <form class="form-group" method="post" action="{{route('register')}}">
                {{csrf_field()}}

                <div class="form-group">
                    <label for="name" class="cols-sm-2 control-label">Your Name<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"
                                                                   aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Enter your Name"
                                   required value="{{old('name')}}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="cols-sm-2 control-label">Your Email<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"
                                                                   aria-hidden="true"></i></span>
                            <input type="email" class="form-control" name="email" placeholder="Enter your Email"
                                   required value="{{old('email')}}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Your Address<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"
                                                                   aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="address" placeholder="Enter your address"
                                   required value="{{old('address')}}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Your Phone<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"
                                                                   aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="phone" placeholder="Enter your phone"
                                   required value="{{old('phone')}}"/>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Birth Day<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"
                                                                   aria-hidden="true"></i></span>
                            <input type="text" class="datepicker form-control" data-date-format="mm-dd-yyyy"
                                   name="birthday" readonly>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="cols-sm-2 control-label">Password<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"
                                                                   aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password"
                                   placeholder="Enter your Password"/>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="confirm" class="cols-sm-2 control-label">Confirm Password<span
                                class="text-danger">*</span></label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"
                                                                   aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password_confirmation"
                                   placeholder="Confirm your Password"/>
                        </div>
                    </div>
                </div>


                <div class="col-sm-12" style="text-align: center">
                    <div style="display: inline-block">
                        <button type="submit" class="btn btn-primary">Register</button>
                    </div>
                    <div style="display: inline-block">
                        <a href="{{route('home')}}" type="button" class="btn btn-danger">Back</a>
                    </div>
                </div>

                <div class="form-group">
                    <div style="width: 50%;margin: 23px 0 0 468px;display: inline-block;">
                        @include('layouts.errors')
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<script>
    $.fn.datepicker.defaults.format = "dd-mm-yyyy";
    $('.datepicker').datepicker();
</script>