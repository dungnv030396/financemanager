<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Rikkeisoft | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a><b>Finance Manage</b> Login</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body" style="height: 300px">
        <p class="login-box-msg">Sign in to start your session</p>

        <form method="POST" action="{{route('login')}}">
            {{csrf_field()}}
            <div class="form-group has-success">
                <input type="text" class="form-control" placeholder="Email" name="email" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-success">
                <input type="password" class="form-control" placeholder="Password" name="password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat" style="margin-left: -16px">Login
                </button>
            </div>
        </form>

        <!-- /.social-auth-links -->
        <div class="col-xs-4">
            <button class="btn btn-success btn-block btn-flat" style="margin-left: -30px"><a style="color: white"
                                                                                             href="{{route('view.register.page')}}">Sign
                    In</a></button>
        </div>
        <div style="margin-top: 63px"><a href="{{route('view.forgot.password.page')}}">I forgot my password</a></div>
        <div class="col-xs-4">
            <button class="btn btn-danger btn-block btn-flat" style="margin: 16px 0 0 223px;"><a style="color: white" href="{{route('login.google')}}">Google</a></button>
        </div>
    </div>
    <div class="form-group">
        @if (\Illuminate\Support\Facades\Session::has('confirmSuccess'))
            <div class="alert alert-success">
                <ul>
                    {{\Illuminate\Support\Facades\Session::get('confirmSuccess')}}
                </ul>
            </div>
        @endif
    </div>
    <div class="form-group">
        @if (\Illuminate\Support\Facades\Session::has('changeSuccess'))
            <div class="alert alert-success">
                <ul>
                    {{\Illuminate\Support\Facades\Session::get('changeSuccess')}}
                </ul>
            </div>
        @endif
    </div>
    <div class="form-group">
        @include('layouts.errors')
    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>
</body>
</html>
