@extends('layouts.master')
@section('content')
    <!-- SweetAlert's library -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" href="{{asset('css/master.css')}}">
                <section>
                <div class="nav-tabs-custom">
                    <h1>Profile Page</h1>
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <div class="row">
                                <div class="col-sm-offset-5">
                                    <form method="POST" action="{{route('update.avatar')}}"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="text-center">
                                                @if(str_contains(\Illuminate\Support\Facades\Auth::user()->avatar,'lh5.googleusercontent.com'))
                                                <img src="{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                                     class="avatar img-circle img-thumbnail" alt="avatar" style="height: 200px">
                                                @else
                                                    <img src="{{ URL::to('/') }}/storage/avatar/{{ \Illuminate\Support\Facades\Auth::user()->avatar }}"
                                                         class="avatar img-circle img-thumbnail" alt="avatar">
                                                @endif
                                                <h6>Change other avatar here!!!</h6>
                                                <input type="file" class="text-center center-block well well-sm"
                                                       name="avatar" id="avatar" required>

                                                <div class="form-group">
                                                    <div class="col-sm-12">
                                                        <button type="submit" class="btn btn-main">Update Avatar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-7" style="margin-top: 20px;text-align: left">
                                       @include('layouts.errors')
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-offset-1 col-sm-11">
                                        @if (\Illuminate\Support\Facades\Session::has('changeInforSuccess'))
                                            <div class="alert alert-success">
                                                <ul>
                                                    {{\Illuminate\Support\Facades\Session::get('changeInforSuccess')}}
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <form class="form-horizontal" method="post" action="{{route('change.information')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">Name <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{\Illuminate\Support\Facades\Auth::user()->name}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-1 control-label">Email <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                               value="{{\Illuminate\Support\Facades\Auth::user()->email}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">Address <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="text" class="form-control" name="address" placeholder="Address"
                                               value="{{\Illuminate\Support\Facades\Auth::user()->address}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">BirthDay <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <div class="input-group date" data-provide="datepicker">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                            <input type="text" class="datepicker form-control"
                                                   data-date-format="mm-dd-yyyy"
                                                   @if(\Illuminate\Support\Facades\Auth::user()->birthday !=null)
                                                   value="{{date('d-m-Y', strtotime(\Illuminate\Support\Facades\Auth::user()->birthday)) }}"
                                                   @endif
                                                   name="birthday" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">Phone <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="text" class="form-control" name="phone" placeholder="Phone"
                                               value="{{\Illuminate\Support\Facades\Auth::user()->phone}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-1 col-sm-11">
                                        <button type="submit" class="btn btn-main">Submit</button>
                                    </div>
                                </div>
                            </form>

                            <hr>

                            <form class="form-horizontal" method="post" action="{{route('change.password')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">Current Pass <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="password" class="form-control" name="current_password"
                                               placeholder="Current Password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">New password <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="password" class="form-control" name="password"
                                               placeholder="New password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-1 control-label">Confirmation <span class="text-red">*</span></label>

                                    <div class="col-sm-11">
                                        <input type="password" class="form-control" name="password_confirmation"
                                               placeholder="Confirmation new password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-1 col-sm-11">
                                        <button type="submit" class="btn btn-main">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-offset-1 col-sm-11">
                                    @if (\Illuminate\Support\Facades\Session::has('changePassSuccess'))
                                        <div class="alert alert-success">
                                            <ul>
                                                {{\Illuminate\Support\Facades\Session::get('changePassSuccess')}}
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-offset-1 col-sm-11 text-left">
                                    @include('layouts.errors')
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
        <script>
            $.fn.datepicker.defaults.format = "dd-mm-yyyy";
            $('.datepicker').datepicker();
        </script>
    </section>

@endsection