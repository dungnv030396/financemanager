@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    {{--<link rel="stylesheet" href="{{asset('css/modal.css')}}">--}}

    <!------ Include the above in your HEAD tag ---------->
    @if(\Illuminate\Support\Facades\Session::has('errorID'))
        @include('sweet::alert')
    @endif

    <div class="container-fluid">
        <div class="row">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">Transfer History Page</h1>
                    <hr/>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-1" style="float: right;margin:0 250px 20px 0">
                    <form action="{{route('search.wallet.name.in.history')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="value" class="form-control pull-right" style="width: 300px"
                                   placeholder="Search Something">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="box">
                    <div class="form-group">
                        <table class="table table-bordered" id="table-department">
                            <tbody>
                            <tr>
                                <th style="width: 10px">Number</th>
                                <th>Wallet Out</th>
                                <th>Wallet In</th>
                                <td>Amount</td>
                                <th style="width: 200px">Created time</th>
                                <th style="width:103px">Action</th>
                            </tr>
                            <?php $i =9 ?>
                            @foreach($transferHistory as $item)
                                <tr>
                                    <td>{{ ($transferHistory->currentPage()*10) - $i }}</td> <?php $i-- ?>
                                    <td><a href="{{ route('view.categories.in.wallet',$item->wallet_out_id) }}" style="color: blue">{{ $item->wallet_out->name }}</a></td>
                                    <td><a href="{{ route('view.categories.in.wallet',$item->wallet_in_id) }}" style="color: blue">{{ $item->wallet_in->name }}</a></td>
                                    <td class="text-income">{{ number_format($item->amount) }}</td>
                                    <td>{{ date('d-m-Y | H:m:s', strtotime($item->created_at)) }}</td>
                                    <td>
                                        <div style="float: left">
                                            <a href="" class="btn btn-sm btn-delete btn-danger" id="{{$item->id}}"
                                               onclick="getConfirmation(this.id)" style="margin-left: 7px"><span
                                                        class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12" style="text-align: center">
                        <div style="display: inline-block">
                            {{ $transferHistory->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function getConfirmation(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Are you sure?",
                text: "Do you want delete history ID: " + id + "?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I want to delete!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: " {{ route('delete.transfer.history') }}",
                        type: 'DELETE',
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function () {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Success", "Deleted History ID: " + id + " Successful");
                            window.location.reload(true);
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection