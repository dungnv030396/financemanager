@extends('layouts.master')
@section('content')
    <!-- SweetAlert's library -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/master.css')}}">


    <section>
        @if(\Illuminate\Support\Facades\Session::has('transferMessage'))
            @include('sweet::alert')
        @endif
        <div class="nav-tabs-custom">
            <h1 class="text-color">Transfer Money</h1>

            <div class="tab-content">
                <div class="tab-pane active" id="settings">
                    <br>
                    <form class="form-horizontal" method="post" action="">
                        {{ csrf_field() }}
                        <div class="form-group" id="wallet_out">
                            <label for="inputName" class="col-sm-1 control-label">Wallet Out <span class="text-red">*</span></label>
                            <div class="col-sm-11">
                                <select class="wallet_out form-control" name="wallet_out" required>
                                    <option value=""></option>
                                    @foreach($wallets as $wallet)
                                        <option value="{{$wallet->id}}">{{$wallet->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="wallet_in">
                            <label for="inputName" class="col-sm-1 control-label">Wallet In <span class="text-red">*</span></label>
                            <div class="col-sm-11">
                                <select class="wallet_in form-control" name="wallet_in" required>
                                    <option value=""></option>
                                    @foreach($wallets as $wallet)
                                        <option value="{{$wallet->id}}">{{$wallet->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Amount <span class="text-red">*</span></label>

                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="amount"
                                       placeholder="Enter Number of money"
                                       value="{{old('amount')}}" required>
                            </div>
                        </div>

                        <div class="col-sm-12" style="text-align: center">
                            <div style="display: inline-block">
                                <button type="submit" class="btn btn-main" style="color: white">Submit</button>
                            </div>
                            <div style="display: inline-block">
                                <a href="{{route('manage.wallets.page','all')}}" type="button" class="btn btn-danger"
                                   style="color: white">Back</a>
                            </div>
                        </div>

                    </form>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11 text-left" style="margin: 20px;">
                                @include('layouts.errors')
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->
                    {{--</div>--}}
                </div>
            </div>
    </section>
    <script>
        $(document).ready(function () {
            $('.wallet_out').select2({
                placeholder: 'Select wallet out name',
            });
            $('.wallet_in').select2({
                placeholder: 'Select wallet in name',
            });
        });

    </script>
@endsection