@extends('layouts.master')
@section('content')
    <!-- SweetAlert's library -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/master.css')}}">
    <section>
        @if(\Illuminate\Support\Facades\Session::has('errorID'))
            @include('sweet::alert')
        @endif
        @if(\Illuminate\Support\Facades\Session::has('updateWalletFail'))
            @include('sweet::alert')
        @endif
        <div class="nav-tabs-custom">
            <h1 class="text-color">Update Wallet ID: <b>{{$wallet->id}}</b></h1>

            <div class="tab-content">
                <div class="tab-pane active" id="settings">
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11">
                                @if (\Illuminate\Support\Facades\Session::has('updateWalletSuccess'))
                                    <div class="alert alert-success">
                                        <ul>
                                            {{\Illuminate\Support\Facades\Session::get('updateWalletSuccess')}}
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <form class="form-horizontal" method="post" action="{{route('update.wallet')}}">
                        {{ csrf_field() }}
                        <input type="text" hidden value="{{$wallet->id}}" name="wallet_id">
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Wallet Name <span class="text-red">*</span></label>

                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="name" placeholder="Enter Wallet Name"
                                       value="{{ $wallet->name }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Notes <span class="text-red">*</span></label>

                            <div class="col-sm-11">
                                <textarea class="form-control" name="notes" placeholder="Enter Wallet's notes" required
                                          rows="4">{{ $wallet->notes }}</textarea>
                            </div>
                        </div>

                        <div class="col-sm-12" style="text-align: center;margin-bottom: 30px">
                            <div style="display: inline-block">
                                <button type="submit" class="btn btn-main" style="color: white">Submit</button>
                            </div>
                            <div style="display: inline-block">
                                <a href="{{route('manage.wallets.page','all')}}" type="button" class="btn btn-danger"
                                   style="color: white">Back</a>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11 text-left">
                                @include('layouts.errors')
                            </div>
                        </div>
                    </div>

                    </div>
                    <!-- /.tab-pane -->
                </div>
            </div>
    </section>

@endsection