@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <!------ Include the above in your HEAD tag ---------->

    <div class="container-fluid">
        <div class="row">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">Manage All Wallets</h1>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1" style="margin: 25px 0 0 30px">
                    <a href="{{route('create.new.wallet.page')}}" type="button" class="btn btn-main"
                       style="margin: -40px 0px 20px -14px"><i class="glyphicon glyphicon-plus"></i> Add New
                        Wallet</a>
                </div>
                <div class="col-sm-1" style="margin: 25px 0 0 30px">
                    <a href="{{route('view.transfer.page')}}" type="button" class="btn btn-main"
                       style="margin: -40px 0px 20px -14px"><i class="glyphicon glyphicon-euro"></i> Transfer</a>
                </div>
                <div class="col-sm-1" style="float: right;margin:0 250px 20px 0">
                    <form action="{{route('search.wallet')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="value" class="form-control pull-right" style="width: 300px"
                                   placeholder="Search Something">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="box">
                    <div class="form-group">
                        <table class="table table-bordered" id="table-department">
                            <tbody>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Wallet Name</th>
                                <th>Amount (vnd)</th>
                                <th>Balance (vnd)</th>
                                <th><b>Total</b> (vnd)</th>
                                <th style="width: 200px">Created time</th>
                                <th style="width: 200px">Updated time</th>
                                <th style="width:17%">Action</th>
                            </tr>
                            @foreach($wallets as $wallet)
                                <tr>
                                    <td>{{ $wallet->id }}</td>
                                    <td>{{ $wallet->name }}</td>
                                    <td>
                                        @if($wallet->amount_wallet <=0)
                                            <span class="text-expense"> {{number_format($wallet->amount_wallet)}}</span>
                                        @elseif($wallet->amount_wallet >0)
                                            <span class="text-income">+{{number_format($wallet->amount_wallet)}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $total = 0; ?>
                                        @foreach($wallet->details as $item)
                                            <?php
                                            if ($item->type_id == 1) {
                                                $total -= $item->amount;
                                            } elseif ($item->type_id == 2) {
                                                $total += $item->amount;
                                            }
                                            ?>

                                        @endforeach
                                        @if($total <=0)
                                            <span class="text-expense"> {{number_format($total)}}</span>
                                        @elseif($total >0)
                                            <span class="text-income">+{{number_format($total)}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($wallet->amount_wallet+ $total <=0)
                                            <span class="text-expense"> {{number_format($wallet->amount_wallet+ $total)}}</span>
                                        @elseif($wallet->amount_wallet+ $total >0)
                                            <span class="text-income">+{{number_format($wallet->amount_wallet+ $total)}}</span>
                                        @endif
                                    </td>
                                    <td>{{ date('d-m-Y | H:m:s', strtotime($wallet->created_at)) }}</td>
                                    <td>{{ date('d-m-Y | H:m:s', strtotime($wallet->updated_at)) }}</td>
                                    <td>
                                        <div style="float: left">
                                            <span><a href="{{route('view.detail.wallet',$wallet->id)}}"
                                                     class="btn btn-sm btn-primary"><span
                                                            class="glyphicon glyphicon-wrench"></span> Edit</a></span>
                                        </div>
                                        <div style="float: left">
                                            <a href="" class="btn btn-sm btn-delete btn-danger" id="{{$wallet->id}}"
                                               onclick="getConfirmation(this.id)" style="margin-left: 10px"><span
                                                        class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                                        </div>
                                        <div style="float: left">
                                            <a href="{{route('view.categories.in.wallet',$wallet->id)}}" class="btn btn-sm btn-success" id="" style="margin-left: 10px"><span
                                                        class="	glyphicon glyphicon-transfer"></span> Transactions</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12" style="text-align: center">
                        <div style="display: inline-block">
                            {{ $wallets->links() }}
                        </div>
                    </div>
                </div>
            </div>
            @if(\Illuminate\Support\Facades\Session::has('errorURL'))
                @include('sweet::alert')
            @endif
            @if(\Illuminate\Support\Facades\Session::has('createWalletSuccess'))
                @include('sweet::alert')
            @endif
            {{--@if(\Illuminate\Support\Facades\Session::has('createSuccess'))--}}
            {{--@include('sweet::alert')--}}
            {{--@endif--}}
        </div>
    </div>

    <script>
        function getConfirmation(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Are you sure?",
                text: "Do you want delete Wallet ID: " + id + "?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I want to delete!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: " {{ route('delete.wallet') }}",
                        type: 'DELETE',
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function () {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Success", "Deleted Wallet ID: " + id + " Successful");
                            window.location.reload(true);
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection