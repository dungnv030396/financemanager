@extends('layouts.master')
@section('content')
    <!-- SweetAlert's library -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/master.css')}}">
    <section>
        @if(\Illuminate\Support\Facades\Session::has('errorID'))
            @include('sweet::alert')
        @endif

        @if(\Illuminate\Support\Facades\Session::has('updateCategorySuccess'))
            @include('sweet::alert')
        @endif
        <div class="nav-tabs-custom">
            <h1 class="text-color">Update Wallet ID: <b>{{$wallet->id}}</b></h1>

            <div class="tab-content">
                <div class="tab-pane active" id="settings">
                    <br>
                    <form class="form-horizontal" method="post" action="{{route('update.categories.in.wallet')}}">
                        {{ csrf_field() }}
                        <input type="text" hidden value="{{$wallet->id}}" name="wallet_id">
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Type</label>

                            <div class="col-sm-1"><label for="inputName"
                                                         style="margin: 6px 9px -3px 20px;">Expense</label>
                                <div class="form-check"><input type="radio" name="type" id="showSelect1" value="expense"
                                                               checked required></div>
                            </div>
                            <div class="col-sm-1"><label for="inputName"
                                                         style="margin: 6px 9px -3px 20px;">Income</label>
                                <div class="form-check"><input type="radio" name="type" id="showSelect2" value="income"
                                                               required></div>
                            </div>
                        </div>
                        <div class="form-group" id="select1">
                            <label for="inputName" class="col-sm-1 control-label">Categories</label>
                            <div class="col-sm-11">
                                <select class="expense-choice1 form-control" name="expense">
                                    <option value=""></option>
                                    @foreach($allCategories as $category)
                                        @if($category->type_id == 1)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group hidden" id="select2">
                            <label for="inputName" class="col-sm-1 control-label">Categories</label>
                            <div class="col-sm-11">
                                <select class="expense-choice2 form-control" name="income">
                                    <option value=""></option>
                                    @foreach($allCategories as $category)
                                        @if($category->type_id == 2)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Amount</label>

                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="amount"
                                       placeholder="Enter number of money"
                                       value="{{old('amount')}}">
                            </div>
                        </div>
                        <div class="col-sm-12" style="text-align: center;margin-bottom: 30px">
                            <div style="display: inline-block">
                                <button type="submit" class="btn btn-main" style="color: white">Submit</button>
                            </div>
                            <div style="display: inline-block">
                                <a href="{{route('manage.wallets.page','all')}}" type="button" class="btn btn-danger"
                                   style="color: white">Back</a>
                            </div>
                        </div>
                    </form>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11 text-left">
                                @include('layouts.errors')
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">


                            <label for="inputName" class="col-sm-1 control-label">Categories</label>
                            <div class="col-sm-11">

                                <table class="table table-bordered">
                                    <tbody>
                                    <tr>
                                        <th style="width: 10px">ID</th>
                                        <th>Category Name</th>
                                        <th>Type</th>
                                        <th>Money (vnd)</th>
                                        <th style="width: 200px">Created time</th>
                                        <th style="width: 200px">Updated time</th>
                                        <th style="width:103px">Action</th>
                                    </tr>
                                    @foreach($dWallet as $item)
                                        <tr>
                                            <td>{{ $item->id }}</td>
                                            <td>{{ $item->category->name }}</td>
                                            <td>
                                                @if($item->type_id==1)
                                                    <span class="text-expense">Expense</span>
                                                @elseif($item->type_id==2)
                                                    <span class="text-income">Income</span>
                                                @endif
                                            </td>
                                            <td>{{ number_format($item->amount) }}</td>
                                            <td>{{ date('d-m-Y | H:m:s', strtotime($item->created_at)) }}</td>
                                            <td>{{ date('d-m-Y | H:m:s', strtotime($item->updated_at)) }}</td>
                                            <td>
                                                <div style="float: left">
                                                    <center><a href="" class="btn btn-sm btn-delete btn-danger"
                                                               id="{{$item->id}}"
                                                               onclick="deleteCategory(this.id)"
                                                               style="margin-left: 7px"><span
                                                                    class="glyphicon glyphicon-floppy-remove"></span>
                                                            Delete</a></center>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-12" style="text-align: center">
                                <div style="display: inline-block">
                                    {{ $dWallet->links() }}
                                </div>
                            </div>
                            <table class="table-total">
                                <tbody>
                                <tr>
                                    <td style="width: 30%;text-align: right"><h3>Amount:</h3></td>
                                    <td>
                                        <h3>
                                            @if($wallet->amount_wallet < 0)<span class="text-expense"> {{number_format($wallet->amount_wallet)}}</span>
                                            @elseif($wallet->amount_wallet >=0)<span class="text-income"> +{{number_format($wallet->amount_wallet)}}</span>@endif
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 30%;text-align: right"><h3>Balance:</h3></td>
                                    <td>
                                        <h3>
                                            @if($total < 0)
                                                <span class="text-expense"> {{number_format($total)}}</span>
                                            @elseif($total >=0)
                                                <span class="text-income"> +{{number_format($total)}}</span>
                                            @endif
                                        </h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: right"><h3><b>Total:</b></h3></td>
                                    <td>
                                        <h3>
                                        @if(($wallet->amount_wallet + $total) < 0)
                                            <span class="text-expense"> {{(number_format($wallet->amount_wallet + $total))}}</span>
                                        @elseif(($wallet->amount_wallet - $total) >=0)
                                            <span class="text-income"> +{{(number_format($wallet->amount_wallet + $total))}}</span>
                                        @endif
                                        </h3>
                                    </td>
                                </tr>
                                </tbody>
                            </table>




                        </div>
                    </div>
                    <!-- /.tab-pane -->
                </div>
            </div>
        </div>
        <script>

            $(".form-check input[type='radio']").on("change", function () {
                if ($("#showSelect1").is(':checked')) {
                    $("#select1").removeClass('hidden');
                    $("#select2").addClass('hidden');
                } else {
                    $("#select1").addClass('hidden');

                    $("#select2").removeClass('hidden');
                    $('.expense-choice2').select2({
                        placeholder: 'Select Income categories name',
                    });
                }
            });

            $(document).ready(function () {
                $('.expense-choice1').select2({
                    placeholder: 'Select Expense categories name',
                });
                $('.expense-choice2').select2({
                    placeholder: 'Select Income categories name',
                });
            });

            function deleteCategory(id) {
                event.preventDefault(); // prevent form submit
                swal({
                    title: "Are you sure?",
                    text: "Do you want Delete Category ID: " + id + "?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, I want to delete!",
                    cancelButtonText: "No, Cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: " {{ route('delete.category.in.wallet') }}",
                            type: 'DELETE',
                            method: "POST",
                            dataType: "json",
                            data: {
                                "_token": "<?= csrf_token() ?>",
                                id: id
                            },
                            success: function () {

                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                swal("Success", "Deleted Category ID: " + id + " Successful");
                                window.location.reload(true);
                            }
                        })
                    } else {
                        swal.close();
                    }
                });
            }
        </script>
    </section>

@endsection