@extends('layouts.master')
@section('content')
    <!-- SweetAlert's library -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/master.css')}}">
    <section>
        <div class="nav-tabs-custom">
            <h1 class="text-color">Create New Wallet</h1>

            <div class="tab-content">
                <div class="tab-pane active" id="settings">
                    <br>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-offset-1 col-sm-11">
                                @if (\Illuminate\Support\Facades\Session::has('createWalletSuccess'))
                                    <div class="alert alert-success">
                                        <ul>
                                            {{\Illuminate\Support\Facades\Session::get('createWalletSuccess')}}
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <form class="form-horizontal" method="post" action="{{route('create.new.wallet')}}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Wallet Name <span
                                        class="text-red">*</span></label>

                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="name" placeholder="Enter Wallet Name"
                                       value="{{old('name')}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Amount <span class="text-red">*</span></label>

                            <div class="col-sm-11">
                                <input type="text" class="form-control" name="amount_wallet"
                                       placeholder="Enter Wallet's Amount"
                                       value="{{old('amount_wallet')}}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputName" class="col-sm-1 control-label">Notes <span class="text-red">*</span></label>

                            <div class="col-sm-11">
                                <textarea class="form-control" name="notes" placeholder="Enter Wallet's notes" required
                                          rows="4"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-12" style="text-align: center">
                            <div style="display: inline-block">
                                <button type="submit" class="btn btn-main" style="color: white">Submit</button>
                            </div>
                            <div style="display: inline-block">
                                <a href="{{route('manage.wallets.page','all')}}" type="button" class="btn btn-danger"
                                   style="color: white">Back</a>
                            </div>
                        </div>

                        <div class="col-sm-offset-1 col-sm-7 text-left" style="margin: 20px 0 0 379px;">
                            @include('layouts.errors')
                        </div>
                    </form>

                    <!-- /.tab-pane -->
                </div>
            </div>
        </div>
    </section>

@endsection