@if (\Illuminate\Support\Facades\Session::has('createCategorySuccess') OR \Illuminate\Support\Facades\Session::has('existCreateName') OR $errors->has('category_name') OR $errors->has('avatar_category'))
    <script>
        $(function () {
            $('#login-modal').modal('show');
        });
    </script>
@endif
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="{{ URL::to('/') }}/images/logo/logo.png">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>
            <div class="div-forms message-success">
                @if (\Illuminate\Support\Facades\Session::has('createCategorySuccess'))
                    <div class="alert alert-success">
                        <ul>
                            {{\Illuminate\Support\Facades\Session::get('createCategorySuccess')}}
                        </ul>
                    </div>
                @endif
            </div>
            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <!-- Begin # Login Form -->
                <form id="login-form" method="POST" action="{{route('create.new.category')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input id="login_username" class="form-control" name="category_name" type="text"
                               placeholder="Enter category name" value="{{ old('category_name') }}" required>
                        <div class="checkbox" style="margin:13px 0 0 -82px">
                            <label>
                                <input type="radio" name="type_name" value="expense" checked> Expense
                            </label>
                            <label>
                                <input type="radio" name="type_name" value="income"> Income
                            </label>
                        </div>
                        <input type="file" class="text-center center-block well well-sm"
                               name="avatar_category" style="margin: 10px 0 0 -1px;">
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Create</button>
                        </div>
                    </div>
                    <div class="div-forms message-success">
                         @include('layouts.errors')
                    </div>
                </form>
                @if(\Illuminate\Support\Facades\Session::has('existCreateName'))
                    @include('sweet::alert')
                @endif
            </div>
        </div>
    </div>
</div>

