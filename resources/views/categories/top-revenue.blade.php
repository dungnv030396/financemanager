@extends('layouts.master')
@section('content')
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{asset('css/modal.css')}}">

    <!------ Include the above in your HEAD tag ---------->

    <div class="container-fluid">
        <div class="row">
            <div class="panel-heading">
                <div class="panel-title text-center" style="margin-top: 58px">
                    <h1 class="title">Transactions</h1>
                    <hr/>
                </div>
            </div>
            <form action="{{ route('view.top.revenue.categories.by.filter') }}" method="POST">
                {{ csrf_field() }}
                <div class="col-sm-12 date-range">
                    <div class="form-group" style="float: left">
                        <label class="label-filter">Date Range:</label>
                        <div class="input-group">
                            <input style="padding: 3px" type="text" name="start_date" placeholder="Format Y-m-d" @if(!is_null(session('start_date'))) value="{{ date('Y-m-d',strtotime(session('start_date')))  }}@endif"> ~
                            <input style="padding: 3px" type="text" name="end_date" placeholder="Format Y-m-d"  @if(!is_null(session('end_date')))value="{{ date('Y-m-d',strtotime(session('end_date'))) }} @endif">
                        </div>
                    </div>
                    <div class="checkbox checkbox-filter">
                        <label><b>Type:</b></label>
                        <label>
                            <div class="form-check"><input type="radio" name="type_name" id="showSelect1" value="1"
                                                           @if( session('type-name')  == 1) checked @endif>
                                Expense
                            </div>
                        </label>
                        <label>
                            <div class="form-check"><input type="radio" name="type_name" id="showSelect2" value="2"
                                                           @if( session('type-name')  == 2) checked @endif>
                                Income
                            </div>
                        </label>
                    </div>
                    <div class="category-filter">
                        {{--<label><b>Categories:</b></label>--}}
                        <div class="form-group hidden" id="select1">
                            <div class="col-sm-11">
                                <select class="expense-choice1 form-control" name="expense"
                                        style="width: 300px;margin-left: 100px">
                                    <option value=""></option>
                                    @foreach($allCategories as $category)
                                        @if($category->type_id == 1)
                                            <option value="{{$category->id}}"
                                                    @if(session('category') == $category->id) selected @endif>{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group hidden" id="select2">
                            <div class="col-sm-11">
                                <select class="expense-choice2 form-control" name="income" style="width: 300px">
                                    <option value=""></option>
                                    @foreach($allCategories as $category)
                                        @if($category->type_id == 2)
                                            <option value="{{$category->id}}"
                                                    @if(session('category') == $category->id) selected @endif>{{$category->name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-filter">
                    <button class="btn btn-main" type="submit">Filter</button>
                </div>

            </form>
            <div class="btn-filter">
                <button class="btn btn-success" id="btn-reset" >Reset</button>
            </div>
            <div class="col-sm-12" style="margin-top: 13px;">
                @include('layouts.errors')
            </div>
            <div class="col-sm-12" style="margin-top: 20px">
                <div class="box">
                    <div class="form-group">
                        <table class="table table-bordered" id="table-department">
                            <tbody>
                            <tr>
                                <th style="width: 10px">Number</th>
                                <th>Category Name</th>
                                <th style="width: 160px">Type</th>
                                <th style="width: 200px">Revenue</th>
                            </tr>
                            <?php $i = 9 ?>
                            @foreach($listCategories as $item)
                                <tr>
                                    <td>{{ ($listCategories->currentPage() *10) - $i }}</td> <?php $i-- ?>
                                    <td><img class="category_avatar"
                                             src="{{ URL::to('/') }}/storage/category_avatar/{{ $item->avatar}} "><span
                                                class="float-left" style="margin: 7px">{{ $item->name }}</span></td>
                                    <td>
                                        @if($item->type_id==1)
                                            <span class="text-expense">Expense</span>
                                        @elseif($item->type_id==2)
                                            <span class="text-income">Income</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->type_id==1)
                                            <span class="text-expense">-{{ number_format($item->total) }}</span>
                                        @elseif($item->type_id==2)
                                            <span class="text-income">+{{ number_format($item->total) }}</span>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12" style="text-align: center">
                        <div style="display: inline-block">
                            {{ $listCategories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="{{asset('js/moment.min.js')}}"></script>
    <script src="{{ asset('js/daterangepicker.js') }}"></script>
    <script src=" {{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#btn-reset').click(function () {
                $("#showSelect1").prop('checked', false);
                $("#showSelect2").prop('checked', false);
                $("#select1").addClass('hidden');
                $("#select2").addClass('hidden');
                $('.expense-choice1').val(null).trigger('change');
                $('.expense-choice2').val(null).trigger('change');
                $('#start_date').val(null);
                $('#end_date').val(null);
            });
        });

        $(".form-check input[type='radio']").on("change", function () {
            if ($("#showSelect1").is(':checked')) {
                $("#select1").removeClass('hidden');
                $("#select2").addClass('hidden');
                $('.expense-choice1').select2({
                    placeholder: 'Select Expense categories name',
                });
            } else {
                $("#select1").addClass('hidden');
                $("#select2").removeClass('hidden');
                $('.expense-choice2').select2({
                    placeholder: 'Select Income categories name',
                });
            }

        });
        $(document).ready(function () {
            $('.expense-choice1').select2({
                placeholder: 'Select Expense categories name',
            });
            $('.expense-choice2').select2({
                placeholder: 'Select Income categories name',
            });

            if ($("#showSelect1").is(':checked')) {
                $("#select1").removeClass('hidden');
                $("#select2").addClass('hidden');

            }
            if ($("#showSelect2").is(':checked')) {
                $("#select1").addClass('hidden');
                $("#select2").removeClass('hidden');
            }
        });

    </script>
@endsection