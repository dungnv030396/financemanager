@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" href="{{asset('css/modal.css')}}">

    <!------ Include the above in your HEAD tag ---------->

    <div class="container-fluid">
        <div class="row">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">Manage All Categories</h1>
                    <hr/>
                </div>
            </div>
            <div style="float: left">
                <a href="#" class="btn btn-main btn-create" role="button" data-toggle="modal"
                   data-target="#login-modal">Create new category</a>
            </div>
            @include('categories.create-new-categories')
            <div class="row">
                <div class="col-sm-1" style="float: right;margin:0 250px 20px 0">
                    <form action="{{route('search.categories')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="value" class="form-control pull-right" style="width: 300px"
                                   placeholder="Search Something">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="box">
                    <div class="form-group">
                        <table class="table table-bordered" id="table-department">
                            <tbody>
                            <tr>
                                <th style="width: 10px">Number</th>
                                <th>Category Name</th>
                                <th style="width: 160px">Type</th>
                                <th style="width: 200px">Created time</th>
                                <th style="width: 200px">Updated time</th>
                                <th style="width:10%;text-align: center">Action</th>
                            </tr>
                            <?php $i = 9 ?>
                            @foreach($categories as $category)
                                <tr>

                                    <td>{{ ($categories->currentPage()*10) - $i }}</td>  <?php $i-- ?>
                                    <td><img class="category_avatar" src="{{ URL::to('/') }}/storage/category_avatar/{{ $category->avatar}} "><span class="float-left" style="margin: 7px">{{ $category->name }}</span></td>
                                    <td>
                                        @if($category->type_id ==1)
                                            <span class="text-expense">Expense</span>
                                        @elseif($category->type_id == 2)
                                            <span class="text-income">Income</span>
                                        @endif
                                    </td>
                                    <td>{{ date('d-m-Y | H:m:s', strtotime($category->created_at)) }}</td>
                                    <td>{{ date('d-m-Y | H:m:s', strtotime($category->updated_at)) }}</td>
                                    <td>
                                        <div style="float: left">
                                            <a href="" class="btn btn-sm btn-primary edit_modal" data-toggle="modal" data-target="#edit-modal" data-content="{{ $category->name }}"
                                               data-title="{{ $category->avatar }}" data-html="{{ $category->id }}" data-placement="{{ $category->type_id }}"
                                               style="margin-left: 7px"><span class="glyphicon glyphicon-wrench"></span> Edit</a>
                                        </div>
                                        <div style="float: left">
                                            <a href="" class="btn btn-sm btn-delete btn-danger" id="{{$category->id}}"
                                               onclick="getConfirmation(this.id)" style="margin-left: 7px"><span
                                                        class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-12" style="text-align: center">
                        <div style="display: inline-block">
                            {{ $categories->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('categories.edit-categories')
    <script>
        function getConfirmation(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Are you sure?",
                text: "Do you want delete Wallet ID: " + id + "?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I want to delete!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: " {{ route('delete.category') }}",
                        type: 'DELETE',
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function () {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Success", "Deleted Wallet ID: " + id + " Successful");
                            window.location.reload(true);
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection