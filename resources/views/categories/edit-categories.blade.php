<div class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content" style="width: 58%;margin-left: 111px;">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>

            <!-- Begin # DIV Form -->
            <div id="div-forms">
                <!-- Begin # Login Form -->
                <form id="login-form" method="POST" action="{{route('edit.categories') }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="text" hidden id="category_id" name="category_id">
                        <input id="category_name" class="form-control" name="name" type="text"
                               placeholder="Enter category name" value="{{ old('category_name') }}" required>
                        <div class="checkbox" style="margin:13px 0 0 -82px">
                            <label>
                                <input type="radio" id="expense_id" name="type_id" value="1" checked> Expense
                            </label>
                            <label>
                                <input type="radio" id="income_id" name="type_id" value="2"> Income
                            </label>
                        </div>
                        <input type="file" class="text-center center-block well well-sm"
                               name="avatar" style="margin: 10px 0 0 -1px;">
                    </div>
                    <div class="modal-footer">
                        <div>
                            <button type="submit" class="btn btn-primary btn-lg btn-block">Update</button>
                        </div>
                    </div>
                    <div class="div-forms message-success">
                         @include('layouts.errors')
                    </div>
                </form>
                @if(\Illuminate\Support\Facades\Session::has('existEditName'))
                    @include('sweet::alert')
                @endif
                @if(\Illuminate\Support\Facades\Session::has('updateCategorySuccess'))
                    @include('sweet::alert')
                @endif
                @if($errors->any())

                @endif
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click", ".edit_modal", function () {
        var category_name = $(this).data('content');
        var avatar = $(this).data('title');
        var id = $(this).data('html');
        var type_id = $(this).data('placement');
        if (type_id===1){
            $(".modal-body #expense_id").prop('checked',true);
        }else{
            $(".modal-body #income_id").prop('checked',true);
        }
        $(".modal-body #category_name").val( category_name );
        $(".modal-body #category_id").val( id );
        $(".modal-header #img_logo").attr( "src","{{ URL::to('/') }}/storage/category_avatar/"+ avatar );
    });
</script>