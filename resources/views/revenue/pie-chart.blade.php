@extends('layouts.master')
@section('content')
    <link rel="stylesheet" href="{{asset('css/chart.css')}}">
    <link rel="stylesheet" href="{{asset('css/modal.css')}}">
    <form action="{{ route('view.pie.chart.filter') }}" style="margin-top: 70px" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="input-group">
                <label><b>Filter By Date:</b></label>
                <input style="padding: 3px;margin-left: 10px" type="text" name="start_date" placeholder="Y-m-d" @if(!is_null(session('start_date_chart'))) value="{{ date('Y-m-d',strtotime(session('start_date_chart')))  }}@endif"> ~
                <input style="padding: 3px" type="text" name="end_date" placeholder="Y-m-d"  @if(!is_null(session('end_date_chart')))value="{{ date('Y-m-d',strtotime(session('end_date_chart'))) }} @endif">
            </div>
        </div>
        <button class="btn btn-main filter-chart-btn" type="submit">Filter</button>
    </form>
    <div class="col-sm-12" style="margin-top: 13px;">
        @include('layouts.errors')
    </div>
    <div class="col-md-6">
        <div class="box box-danger border-chart">
            <div class="box-header with-border">
                <h3 class="box-title">Expense</h3>
            </div>
            <div class="box-body" style="height:auto">
                <canvas id="pieChartExpense"></canvas>
            </div>
            @for($i=0;$i<count($pieDataArrExpense);$i++)
                <div class="chart-description-div" style="background-color: {{ $pieDataArrExpense[$i]['color'] }}; border: 1px solid {{ $pieDataArrExpense[$i]['color'] }};">
                <label><b class="chart-description-label">{{ substr($pieDataArrExpense[$i]['label'],0,12) }}</b></label></div>
                <label style="float: left;margin: 52px 0 0 -96px"><b>{{ number_format($pieDataArrExpense[$i]['value']) }}</b></label>
            @endfor
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-danger border-chart">
            <div class="box-header with-border">
                <h3 class="box-title">Income</h3>
            </div>
            <div class="box-body">
                <canvas id="pieChartIncome" style="height:250px"></canvas>
            </div>
            @for($i=0;$i<count($pieDataArrIncome);$i++)
                <div class="chart-description-div" style="background-color: {{ $pieDataArrIncome[$i]['color'] }}; border: 1px solid {{ $pieDataArrIncome[$i]['color'] }};">
                    <label><b class="chart-description-label">{{ substr($pieDataArrIncome[$i]['label'],0,12) }}</b></label></div>
                <label style="float: left;margin: 52px 0 0 -96px"><b>{{ number_format($pieDataArrIncome[$i]['value']) }}</b></label>
            @endfor
        </div>
    </div>
    @include('revenue.line-chart')

    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="{{asset('js/Chart.js')}}"></script>
    <script>
        var pieChartExpense = $('#pieChartExpense').get(0).getContext('2d');
        var pieChartIncome = $('#pieChartIncome').get(0).getContext('2d');
        var pieChartEx = new Chart(pieChartExpense);
        var pieChartIn = new Chart(pieChartIncome);

        var PieDataExpense = {!! $pieDataExpenseJson !!};

        var PieDataIncome = {!! $pieDataIncomeJson !!};
        // console.log(PieData);
        var pieOptions = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke: true,
            //String - The colour of each segment stroke
            segmentStrokeColor: '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth: 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps: 100,
            //String - Animation easing effect
            animationEasing: 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate: true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale: false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //String - A legend template
            legendTemplate: '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
    }
            pieChartEx.Doughnut(PieDataExpense, pieOptions)
            pieChartIn.Doughnut(PieDataIncome, pieOptions)
    </script>
@endsection