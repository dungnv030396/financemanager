<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email');
            $table->integer('email_verified_at')->nullable()->default(0);
            $table->string('password');
            $table->string('avatar');
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->dateTime('birthday')->nullable();
            $table->integer('status')->nullable()->default(1);
            $table->string('token_confirm_password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
